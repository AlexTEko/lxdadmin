'use strict';

angular.module('lxdAdmin.containers')
    .factory('imagesService', ['$http', '$q', '$timeout','settings',
        function ($http, $q, $timeout, settings) {
            var obj = {};

            obj.isOperationFinished = function (operationUrl) {
                return $http.get(settings.apiUrl + operationUrl + '/wait');
            };


            // Get all containers,  including detailed data
            obj.getAll = function () {
                // Sync
                return $http.get(settings.apiUrl + settings.version + '/images').then(function (data) {
                    data = data.data;

                    if (data.status != "Success") {
                        return $q.reject("Error");
                    }

                    var promises = data.metadata.map(function (imagesUrl) {
                        return $http.get(settings.apiUrl + imagesUrl).then(function (resp) {
                            return resp.data.metadata;
                        });
                    });

                    return $q.all(promises);
                });
            };

            obj.delete = function (imageName) {
                return $http.delete(settings.apiUrl + settings.version + '/images/' + imageName).then(function (data) {
                    return data;
                });
            };

            return obj;
        }])
;
