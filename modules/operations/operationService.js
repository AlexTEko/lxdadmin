'use strict';

angular.module('lxdAdmin.operation')
        .factory('operationService', ['$http', '$q', 'settings',
            function ($http, $q, settings) {
              var obj = {};

                // // Get a operation
                // obj.get = function (operationName) {
                //     return $http.get(SettingServices.getLxdApiUrl() + '/operations/' + operationName);
                // }

                // Get all operations
                obj.getAll = function() {
                    return $http.get(settings.apiUrl + settings.version + '/operations',{ignoreLoadingBar: true}).then(function (data) {
                      data = data.data;

                      if (data.status != "Success") {
                        return $q.reject("Error");
                      }

                      // if ( ! _.isEmpty(data.metadata.running)) {

                      if (data.metadata.running) {
                        var promises = data.metadata.running.map(function(operationUrl) {
                            return $http.get(settings.apiUrl + operationUrl,{ignoreLoadingBar: true}).then(function(resp) {
                                return resp.data.metadata;
                            });
                        });

                        return $q.all(promises);
                      }

                      if (data.metadata.failure) {
                        var promises = data.metadata.failure.map(function(operationUrl) {
                            return $http.get(settings.apiUrl + operationUrl).then(function(resp) {
                                return resp.data.metadata;
                            });
                        });

                        return $q.all(promises);
                      }

                      // }

                      //
                      // if ( ! _.isEmpty(data.metadata.failure)) {
                      //
                      //   var promises = data.metadata.failure.map(function(operationUrl) {
                      //       return $http.get(settings.apiUrl + operationUrl).then(function(resp) {
                      //           return resp.data.metadata;
                      //       });
                      //   });
                      //
                      //   return $q.all(promises);
                      // }


                    });
                }

                return obj;
            }])
;
