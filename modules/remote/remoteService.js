'use strict';

angular.module('lxdAdmin.remote')
    .factory('remoteService', ['$http', '$q', '$timeout','settings',
        function ($http, $q, $timeout, settings) {
            var obj = {};

            obj.isOperationFinished = function (operationUrl) {
                return $http.get(settings.apiUrl + operationUrl + '/wait');
            };

            obj.downloadImage = function (image) {
              var data =
              {
                  "public": true,
                  "auto_update": true,
                  "source": {
                      "type": "image",
                      "mode": "pull",
                      "server": settings.remoteUrl,
                      "protocol": "lxd",
                      "fingerprint": image.fingerprint
                  }
              }

              return $http.post(settings.apiUrl + settings.version + '/images', data);
            };

            // Get all containers,  including detailed data
            obj.getAll = function () {
                // Sync
                return $http.get(settings.remoteProxy + settings.remoteversion + '/images').then(function (data) {
                    data = data.data;

                    if (data.status != "Success") {
                        return $q.reject("Error");
                    }

                    var promises = data.metadata.map(function (imagesUrl) {
                        return $http.get(settings.remoteProxy + imagesUrl, { cache: true}).then(function (resp) {
                            // console.log(resp);
                            return resp.data.metadata;
                        })
                        .catch(function (data) {
                          console.log(data);
                          return data;
                        });
                    });

                    return $q.all(promises);
                });
            };

            return obj;
        }])
;
