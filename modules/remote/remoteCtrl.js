'use strict';

angular.module('lxdAdmin.remote', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/remote', {
            title: 'Remote Images',
            templateUrl: 'modules/remote/remote.html',
            controller: 'remoteListCtrl'
        })

        ;
    }])

    .controller('remoteListCtrl', function($scope, $http, $interval, remoteService, $uibModal) {

      $scope.buttons_state = true;
      $scope.loading = true;

      $scope.archList = [
                'amd64',
                'x86_64',

                'i386',
                'i686',

                'armhf',
                'arm64',
                'ppc64el',

                'armv7l',
                'ppc',
                's390x'
              ];

      remoteService.getAll().then(function(images) {
        $scope.images = images;
        $scope.buttons_state = false;
        $scope.loading = false;
      });

      // $scope.buttons_state = false;

      $scope.download = function (image) {
        $scope.buttons_state = true;
        remoteService.downloadImage(image).then(function(data) {
          var operationUrl = data.data.operation;
            remoteService.isOperationFinished(operationUrl).then(function(data) {
                $scope.buttons_state = false;
            });
          });
    }

  })

  // .controller('genericImageModalCtrl', function ($scope, $routeParams, $filter, $location, $uibModalInstance,
  //                                                    image, imagesService) {
  //     $scope.image = image;
  //
  //     $scope.ok = function () {
  //         $uibModalInstance.close($scope.image);
  //     };
  //
  //     $scope.cancel = function () {
  //         $uibModalInstance.dismiss('cancel');
  //     };
  // })

  ;
