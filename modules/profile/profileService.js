'use strict';

angular.module('lxdAdmin.profile')
        .factory('profileService', ['$http', '$q', 'settings',
            function ($http, $q, settings) {
              var obj = {};

                obj.getAll = function() {
                    return $http.get(settings.apiUrl + settings.version + '/profiles').then(function (data) {
                      data = data.data;

                      if (data.status != "Success") {
                        return $q.reject("Error");
                      }

                      var promises = data.metadata.map(function(profileUrl) {
                          return $http.get(settings.apiUrl + profileUrl).then(function(resp) {
                              return resp.data.metadata;
                          });
                      });

                      return $q.all(promises);
                    });
                }
                return obj;
            }])
;
