'use strict';

angular.module('lxdAdmin.terminal', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/terminal/:containerName/:terminalType', {
            title: 'Terminal',
            templateUrl: 'modules/terminal/terminal.html',
            controller: 'terminalCtrl',
        })
        ;
    }])


    .controller('terminalCtrl', function ($scope, $routeParams, $filter, $location,
                                                 terminalService) {
     $scope.containerName = $routeParams.containerName;
     var container = {};

     // get JS terminal emulator
     container.terminal = terminalService.getJavascriptTerminal();
     container.terminal.open(document.getElementById('console'));

     var initialGeometry = container.terminal.proposeGeometry();
     console.log("Rows: " + initialGeometry.rows + " Cols: " + initialGeometry.cols);

     terminalService.getTerminal2($scope.containerName, $routeParams.terminalType, container.terminal, initialGeometry).then(function(term) {
         container.terminal.fit();
     });

    })
;
