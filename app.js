'use strict';

angular.module('lxdAdmin', [
  'ngRoute',
  'ui.bootstrap',
  // 'ui.select',
  'angular-loading-bar',

  'lxdAdmin.containers',
  'lxdAdmin.images',
  'lxdAdmin.terminal',
  'lxdAdmin.remote',
  'lxdAdmin.profile',
  'lxdAdmin.operation'
]).

config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/containers'});
}]).

constant('settings', {
  apiUrl: '/api',
  version: '/1.0',
  remoteProxy: '/proxy',
  remoteUrl: 'https://uk.images.linuxcontainers.org',
  remoteversion: '/1.0',
});
