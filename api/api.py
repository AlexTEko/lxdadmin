from flask import Flask
from flask import Response
from flask import request

from io import BytesIO

import pycurl
import subprocess
import os
import json

app = Flask(__name__)

def send(body = ''):
	if body:
		resp = Response(body)
	else:
		resp = Response()
	resp.headers['Content-Type'] = 'application/json'
	resp.headers['Access-Control-Allow-Origin'] = '*'
	resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
	resp.headers['Access-Control-Allow-Headers'] = 'Content-Type'
	return resp

def curl_get(url):
	buffer = BytesIO()
	c = pycurl.Curl()
	c.setopt(c.URL, 'a' + url)
	c.setopt(c.UNIX_SOCKET_PATH, '/var/lib/lxd/unix.socket')
	c.setopt(c.WRITEDATA, buffer)
	c.perform()
	c.close()
	body = buffer.getvalue()
	return send(body.decode())

def curl_post(url, data):
	buffer = BytesIO()
	c = pycurl.Curl()
	c.setopt(c.URL, 'a' + url)
	c.setopt(c.UNIX_SOCKET_PATH, '/var/lib/lxd/unix.socket')
	c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
	# c.setopt(pycurl.POST, 1)
	c.setopt(c.POSTFIELDS, data)
	c.setopt(c.WRITEDATA, buffer)
	c.perform()
	c.close()
	body = buffer.getvalue()
	return send(body.decode())

def curl_put(url, data):
	buffer = BytesIO()
	c = pycurl.Curl()
	c.setopt(c.URL, 'a' + url)
	c.setopt(c.UNIX_SOCKET_PATH, '/var/lib/lxd/unix.socket')
	c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
	# c.setopt(pycurl.POST, 1)
	c.setopt(c.CUSTOMREQUEST, "PUT")
	c.setopt(c.POSTFIELDS, data)
	c.setopt(c.WRITEDATA, buffer)
	c.perform()
	c.close()
	body = buffer.getvalue()
	return send(body.decode())

def curl_delete(url):
	buffer = BytesIO()
	c = pycurl.Curl()
	c.setopt(c.URL, 'a' + url)
	c.setopt(c.UNIX_SOCKET_PATH, '/var/lib/lxd/unix.socket')
	# c.setopt(pycurl.POST, 1)
	c.setopt(c.CUSTOMREQUEST, "DELETE")
	c.setopt(c.WRITEDATA, buffer)
	c.perform()
	c.close()
	body = buffer.getvalue()
	return send(body.decode())

def curl_delete(url):
	buffer = BytesIO()
	c = pycurl.Curl()
	c.setopt(c.URL, 'a' + url)
	c.setopt(c.UNIX_SOCKET_PATH, '/var/lib/lxd/unix.socket')
	# c.setopt(pycurl.POST, 1)
	c.setopt(c.CUSTOMREQUEST, "DELETE")
	# c.setopt(c.POSTFIELDS, data)
	c.setopt(c.WRITEDATA, buffer)
	c.perform()
	c.close()
	body = buffer.getvalue()
	return send(body.decode())

def create_ssh_forwarding(container):
	output = subprocess.check_output(["/var/www/lxdui/api/forward.sh", container])
	return send(output.decode())

def get_ssh_forwarding(container):
	output = subprocess.check_output(["screen -ls | grep " + container + " | cut -d. -f2 | awk '{print $1}' | cut -d'>' -f2"],shell=True)
	# data = '{"host":"' + container  + '"'
	data = '{"ports":' + json.dumps(output.decode().split('\n'))  + '}'
	return send(data)

@app.route("/")
def hello():
	return 'ping'

@app.route('/1.0/certificates', methods=['GET', 'POST'])
def certificates():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/certificates')

@app.route('/1.0/containers', methods=['GET', 'POST', 'OPTIONS'])
def containers():
	if request.method == 'POST':
		return curl_post('/1.0/containers', request.data)
	if request.method == 'GET':
		return curl_get('/1.0/containers')

	if request.method == 'OPTIONS':
		return send()

@app.route('/1.0/events', methods=['GET', 'POST'])
def events():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/1.0/events')

@app.route('/1.0/images', methods=['GET', 'POST'])
def images():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/1.0/images')

@app.route('/1.0/images/<fingerprint>', methods=['GET', 'POST'])
def image(fingerprint):
	if request.method == 'GET':
		return curl_get('/1.0/images/' + fingerprint)

@app.route('/1.0/images/aliases', methods=['GET', 'POST'])
def aliases():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/1.0/images/aliases')

@app.route('/1.0/networks', methods=['GET', 'POST'])
def networks():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/1.0/networks')

@app.route('/1.0/operations', methods=['GET', 'POST'])
def operations():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/1.0/operations')

@app.route('/1.0/operations/<uuid>/wait', methods=['GET'])
def operation_wait(uuid):
	if request.method == 'GET':
		return curl_get('/1.0/operations/' + uuid + '/wait')

@app.route('/1.0/operations/<uuid>', methods=['GET'])
def operation(uuid):
	if request.method == 'GET':
		return curl_get('/1.0/operations/' + uuid)

@app.route('/1.0/profiles', methods=['GET', 'POST'])
def profiles():
	if request.method == 'POST':
		return send('not implemented')
	else:
		return curl_get('/1.0/profiles')

@app.route('/1.0/containers/<container>', methods=['GET', 'DELETE', 'OPTIONS'])
def container(container):
	if request.method == 'GET':
		return curl_get('/1.0/containers/' + container)
	if request.method == 'DELETE':
		return curl_delete('/1.0/containers/' + container)
	if request.method == 'OPTIONS':
		return send()

@app.route('/1.0/containers/<container>/state', methods=['GET', 'PUT', 'OPTIONS'])
def state_container(container):
	if request.method == 'GET':
		return curl_get('/1.0/containers/' + container + '/state')
	if request.method == 'PUT':
		return curl_put('/1.0/containers/' + container + '/state', request.data)
	if request.method == 'OPTIONS':
		return send()

@app.route('/1.0/containers/<container>/ssh', methods=['GET', 'PUT','DELETE', 'OPTIONS'])
def ssh_container(container):
	if request.method == 'GET':
		return get_ssh_forwarding(container)
	if request.method == 'PUT':
		return create_ssh_forwarding(container)
	if request.method == 'DELETE':
		return delete_ssh_forwarding(container)
	if request.method == 'OPTIONS':
		return send()

if __name__ == "__main__":
    app.run(host='0.0.0.0')
